#!/usr/bin/make -f

# Makefile for TCOS
#  (C) Jörn Frenzel   <j.frenzel@openthinclient.com> 2013-2018
#  (C) Steffen Hoenig <s.hoenig@openthinclient.com>  2013-2015
#
# License: GPL V2
#

SHELL := /bin/bash 
PATH := /usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:/bin:/sbin

HOST_ARCH := $(shell file /sbin/fdisk)

ifeq (80386,$(findstring 80386,$(HOST_ARCH)))
        HOST_ARCH=i386
endif
ifeq (x86-64,$(findstring x86-64,$(HOST_ARCH)))
        HOST_ARCH=x86_64
endif
TARGET_ARCH := i386
TOP_DIR := $(dir $(lastword $(MAKEFILE_LIST)))

#base-build-BASE_VERSION-BASE_VERSION_MINOR.log
#base-build-2.1-28.log

BASE_VERSION ?= 2018
BASE_VERSION_MINOR ?= .1
#BUSYBOX_VERSION = 1.25.1
BUSYBOX_BRANCH = 1_26_stable
#DEB_MIRROR ?= http://localhost/debian
DEB_MIRROR ?= http://http.debian.net/debian

#LOCAL_TEST_PATH ?= $(shell Scripts/TCOS.ini_parser -r ~/.tcosconfig):/opt/openthinclient/server/default/data/nfs/root
LOCAL_TEST_PATH ?= root@lb-test.dd.otc:/home/openthinclient/otc-manager-home/nfs/root

FIRMWARE_PACKAGES = 3com acenic adaptec amdgpu amd-ucode bnx2 bnx2x e100 intel matrox nvidia radeon RTL8192E rtl_bt rtl_nic tigon i915

TARGET_KERNEL_DEFAULT = 3.16.0-0.bpo.4-686-pae
TARGET_KERNEL_NONPAE = 3.16.0-0.bpo.4-586
TARGET_KERNEL_64 = 4.9.0-0.bpo.4-amd64
TARGET_KERNEL = $(TARGET_KERNEL_DEFAULT) $(TARGET_KERNEL_NONPAE)  $(TARGET_KERNEL_64)

# A stupid way to show all packages from a specific repo.
# grep ^Package: /var/lib/apt/lists/http.debian.net_debian_dists_wheezy-backports* | awk '{print $NF}' | sort -u

# todo
# clean up Sources/tcos/etc/xdg_old
# 

# run-time packages
#
# Wheezy
#
TARGET_PACKAGES += alsa-utils aptitude apt-utils ca-certificates console-data console-tools coreutils dbus dbus-x11 dconf-tools dialog dmidecode dnsutils dos2unix dosfstools e2fsprogs eject ethtool file fontconfig gdevilspie gvfs gvfs-backends htop iproute2 iputils-ping ipython ldap-utils less libcurl3 libgssglue1 libjpeg62 libpam-ldap libsasl2-modules libsasl2-modules-gssapi-mit libssl1.0.0 libstdc++5 libwebkitgtk-1.0-0 libx11-6 libxerces-c3.1 lightdm lightdm-gtk-greeter lshw ltrace man-db mc net-tools nfs-common ntp numlockx openssh-client openssh-server parted pciutils python-gconf python-gtk2 python-ldap python-xdg rsync screen strace sudo squashfs-tools syslog-ng ttf-dejavu vim vim-tiny xdg-utils xfonts-base xinetd libnotify-bin unzip python-crypto xvkbd  libxfce4ui-1-0  libexo-1-0 libxcb-util0 libxfce4util4  libxfce4util-bin  libthunarx-2-0 xfdesktop4 wmctrl python-netifaces python-psutil clipit shutter telnet thunar xfwm4 xfce4-panel xfce4-session xfce4-settings source-highlight python-qt4 pavucontrol pulseaudio pulseaudio-utils pulseaudio-module-x11 gvfs gvfs-backends 

# libdrm-nouveau1a 

TARGET_PACKAGES_BACKPORTS += atril caja engrampa eom mate-applets mate-desktop mate-media mate-media-pulse mate-screensaver mate-session-manager mate-system-monitor mate-themes mate-utils pluma


#####################
TARGET_PACKAGES_JESSIE_OTHER += arandr locales locales-all read-edid libgl1-mesa-dri libgl1-mesa-glx libpcsc-perl libuuid-perl mesa-utils virtualbox-guest-x11 firmware-linux firmware-linux-free firmware-linux-nonfree libavcodec-extra-56 libavcodec-extra57 libav-tools libiomp5 gir1.2-gstreamer libgstreamer1.0-dev rdesktop zenity devilspie devilspie2 nwipe wpasupplicant isc-dhcp-client binutils usbutils efibootmgr apt aptitude gtkperf libmotif3 libmotif4 libappindicator1 ntfs-3g haveged gstreamer1.0-alsa gstreamer1.0-crystalhd gstreamer1.0-fluendo-mp3 gstreamer1.0-gnonlin gstreamer1.0-libav gstreamer1.0-nice gstreamer1.0-plugins-bad gstreamer1.0-plugins-base gstreamer1.0-plugins-base-apps gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly gstreamer1.0-pulseaudio gstreamer1.0-tools gstreamer1.0-vaapi gstreamer1.0-x gstreamer1.0-clutter cheese libxp6 xinput xinput-calibrator dosfstools e2fsprogs file spice-client tcpdump udhcpc  util-linux wget x11vnc xtightvncviewer xdotool mate-power-manager cifs-utils  hwinfo firefox-esr-l10n-de xtightvncviewer libxdo3 xdotool wget x11vnc parted hwinfo hdparm python-configparser libnspr4 libnss3 mpv 

# REMEBER:  xserver-xorg-video-all does not really contain all the xorg-video modules
#

TARGET_PACKAGES_JESSIE_X += arandr libdrm-radeon1 libdrm2 libdrm-intel1 libdrm-amdgpu1 x11-xserver-utils xorg xserver-xorg xserver-xorg-core xserver-xorg-input-evdev xserver-xorg-input-multitouch xserver-xorg-input-mutouch xserver-xorg-input-wacom xserver-xorg-video-all xserver-xorg-video-tdfx xserver-xorg-video-trident xserver-xorg-video-savage xserver-xorg-video-r128 xserver-xorg-video-neomagic xserver-xorg-video-nvidia xserver-xorg-video-cirrus xserver-xorg-video-geode xserver-xorg-video-glide xserver-xorg-video-intel xserver-xorg-video-ivtv xserver-xorg-video-mach64 xserver-xorg-video-mga xserver-xorg-video-nvidia-legacy-340xx xserver-xorg-video-nvidia-legacy-304xx  xserver-xorg-video-sisusb xserver-xorg-video-nvidia xserver-xorg-video-amdgpu xserver-xorg-video-radeon xserver-xorg-input-elographics  mesa-vdpau-drivers vdpauinfo vdpau-va-driver libvdpau-va-gl1 i965-va-driver

TARGET_PACKAGES_JESSIE_FGLRX += fglrx-driver glx-alternative-fglrx glx-diversions libfglrx-amdxvba1 amd-opencl-icd libfglrx

TARGET_PACKAGES_JESSIE_NVIDIA += glx-alternative-nvidia libgl1-nvidia-glx libgl1-nvidia-legacy-304xx-glx libgl1-nvidia-legacy-340xx-glx libnvidia-glcore libnvidia-legacy-304xx-glcore libnvidia-legacy-304xx-ml1 libnvidia-legacy-340xx-glcore libnvidia-legacy-340xx-ml1 libnvidia-ml1 nvidia-alternative nvidia-driver-bin nvidia-installer-cleanup nvidia-legacy-304xx-alternative nvidia-legacy-304xx-driver-bin nvidia-legacy-340xx-alternative nvidia-legacy-340xx-driver-bin nvidia-legacy-check nvidia-support nvidia-vdpau-driver xserver-xorg-video-nvidia xserver-xorg-video-nvidia-legacy-304xx xserver-xorg-video-nvidia-legacy-340xx libvdpau1 nvidia-legacy-304xx-vdpau-driver nvidia-legacy-340xx-vdpau-driver 

TARGET_PACKAGES_JESSIE_SMARTCARD = libccid libpcsclite1 libacsccid1 libasedrive-usb libgempc430 libacr38u libasedrive-serial libpcsc-perl pcsc-tools libpcscada0.7.2 fxcyberjack

# ToDo download from manufactor libifd-cyberjack6 

TARGET_PACKAGES_JESSIE_SNMP = snmpd snmp

# This two packages do some autogeneration (triggerd in thier postinstall scripts) of config files in /etc/reader.conf.d/.
# This config files cause pcscd to crash. 
# libgempc410 libgcr410

PACKAGES_WHEEZY_KHEADER     	 += linux-headers-$(TARGET_KERNEL_DEFAULT) linux-headers-$(TARGET_KERNEL_NONPAE)
PACKAGES_WHEEZY_DKMS        	 += nvidia-legacy-96xx-kernel-dkms nvidia-legacy-173xx-kernel-dkms
PACKAGES_JESSIE_KHEADER		 += linux-headers-$(TARGET_KERNEL_64)
PACKAGES_DKMS_DRVBUILD 		 += gcc dkms make libc6-dev 
PACKAGES_JESSIE_DKMS_FIRST_STEP	 += fglrx-modules-dkms virtualbox-guest-dkms
PACKAGES_JESSIE_DKMS_SECOND_STEP += nvidia-kernel-dkms nvidia-legacy-304xx-kernel-dkms nvidia-legacy-340xx-kernel-dkms
PACKAGES_JESSIE_DMKS_BUILD64     += gcc-4.9 gcc-4.9-base:amd64 linux-headers-4.9.0-0.bpo.4-amd64:amd64

####
# At this moment (Dec. 2017) these module fails to build via dkms under 64 Bit: virtualbox-guest-dkms 

# This list should never be used for install, just for uninstall
#
TARGET_PACKAGES_DELETE += gcc dkms make gcc libc6-dev linux-headers-$(TARGET_KERNEL_64) nvidia-kernel-dkms nvidia-legacy-304xx-kernel-dkms nvidia-legacy-340xx-kernel-dkms virtualbox-guest-dkms linux-headers-$(TARGET_KERNEL_DEFAULT) linux-headers-$(TARGET_KERNEL_NONPAE) fglrx-kernel-dkms:amd64 fglrx-kernel-dkms libc-dev-bin libc6-dev libglib2.0-dev libgstreamer1.0-dev libpcre3-dev linux-libc-dev zlib1g-dev


#####################
TARGET_PACKAGES_DEB := openthinclient-icon-theme_1-3_all.deb  libccid_1.4.28-1_i386.deb gdisk_1.0.1-1_i386.deb libifd-cyberjack6_3.99.5final.sp11_i386_d08.deb
# libccid_1.4.28-1_i386.deb is taken from stretch, see HILFESTELLUNG-930

TARGET_PACKAGES_DEB_AS_DEP := $(addprefix ./Packages/, $(TARGET_PACKAGES_DEB))

RC_SCRIPT_DELETE += hdparm hwclock.sh checkroot.sh checkroot-bootclean.sh checkfs.sh mountall.sh mountall-bootclean.sh mountnfs.sh mountnfs-bootclean.sh alsa-utils  bootmisc.sh  nfs-common rpcbind
# RC_SCRIPT_DELETE += hdparm hwclock.sh checkroot.sh checkroot-bootclean.sh checkfs.sh mountall.sh mountall-bootclean.sh mountnfs.sh mountnfs-bootclean.sh alsa-utils  bootmisc.sh  screen-cleanup 

DISABLED_SERVICES += pcscd haveged 

##
# meta-targets
#

all:  base-release-stamp
test: compressed-stamp upload-test

.PHONY: dist-clean clean chroot chroot-ro help filesystem busybox tcosify update kernel initrd compressed  base upload upload-test

chroot: filesystem-stamp
	@sudo BIND_ROOT=./ Scripts/TCOS.chroot ./Filesystem $(SHELL)

chroot-ro: filesystem-stamp
	@sudo AUFS=1 BIND_ROOT=./ Scripts/TCOS.chroot ./Filesystem $(SHELL)
help:
	@echo "[1mWELCOME TO THE TCOS BUILD SYSTEM[0m"
	@echo ""
	@echo "make all		Metatarget: create the whole system"
	@echo ""
	@echo "make filesystem		Bootstrap and fill the TCOS ./Filesystem directory"
	@echo "make busybox		(Re-)Build Busybox"
	@echo "make kernel		(Re-)Build Kernel packages"
	@echo "make initrd		(Re-)Build Initramfs"
	@echo "make chroot		Work inside Filesystem"
	@echo "make chroot-ro		Work inside Filesystem but don't save changes."
	@echo "make update		Install or update required packages in TCOS ./Filesystem"
	@echo "make compressed		Compress base filesystem image."
	@echo
	@echo "Don't worry about the sequence of build commands, this Makefile will tell you"
	@echo "what to do first, in case anything is missing."
	@echo
	@echo "Have a lot of [36mfun[0m. ;-)"

# build-targets
#
filesystem:
	make $@-stamp
filesystem-stamp:
	@echo "[1m Target filesystem-stamp: Creating an initial filesystem[0m"
	-rm -f tcosify-stamp update-stamp kernel-stamp clean-stamp
	TARGET_ARCH=$(TARGET_ARCH) ./Scripts/TCOS.mkfilesystem $(DEB_MIRROR)
	@touch $@

tcosify:
	make $@-stamp
tcosify-stamp:filesystem-stamp
	@echo "[1m Target tcosify-stamp: Applying TCOS specific changes[0m"
	-rm -f update-stamp kernel-stamp clean-stamp
	Scripts/LINBO.apply-configs Sources/tcos Filesystem
	sudo BASE_VERSION=$(BASE_VERSION) BASE_VERSION_MINOR=$(BASE_VERSION_MINOR) DEB_MIRROR=$(DEB_MIRROR) Scripts/TCOS.chroot Filesystem $(SHELL) < Scripts/TCOS.tcosify-chroot
	@touch $@

update:
	make $@-stamp

update-stamp:tcosify-stamp
	@echo "[1m Target $@-stamp [0m"
	-rm -f clean-stamp
	PATH=$(PATH) sudo BIND_ROOT=./ Scripts/TCOS.chroot Filesystem $(SHELL) -c "\
		set -e ;\
                source /TCOS/Initrd/init_functions ;\
		export DEBIAN_FRONTEND=noninteractive ;\
	        echo \$$GREEN  \" ############### installing wheezy packages ##################### \" \$$NORMAL ;\
		tcos-enable_wheezy ;\
		apt-get -y dist-upgrade && sleep 1 ;\
		apt-get install -y --force-yes --no-install-recommends -t wheezy \
			-o Dpkg::Options::=\"--force-confold\" $(TARGET_PACKAGES) ;\
                apt-get install -y --force-yes --no-install-recommends -t wheezy-backports \
			-o Dpkg::Options::=\"--force-confold\" $(TARGET_PACKAGES_BACKPORTS) ;\
		c_rehash /usr/share/ca-certificates/mozilla ;\
		cd /usr/lib ;\
		ln -s /lib/i386-linux-gnu/libpcre.so.3.13.1 libpcre.so.0 ;\
		ln -s /lib/i386-linux-gnu/libpng12.so.0.49.0 libpng14.so.14 ;\
		ln -s /usr/lib/i386-linux-gnu/libxcb-util.so.0.0.0 libxcb-util.so.1 ;\
		ln -s /usr/lib/libxfce4util.so.4.1.1 libxfce4util.so.6 ;\
		(/etc/init.d/nfs-common stop || true) \
	"
#	the c_rehash is important for citrix packages
#	the link to libs are used by the xfce windowmanager
	@touch $@


update-jessie:
	make $@-stamp

update-jessie-stamp: update-stamp 
	@echo "[1m Target $@-stamp [0m"
	-rm -f clean-stamp
        # AUFS=1
	PATH=$(PATH) sudo  BIND_ROOT=./ Scripts/TCOS.chroot Filesystem $(SHELL) -c "export DEBIAN_FRONTEND=noninteractive ;\
		set -e ;\
                source /TCOS/Initrd/init_functions ;\
		tcos-enable_jessie ;\
	        echo \$$GREEN  \" ############### installing jessie packages ##################### \" \$$NORMAL ;\
		apt-get install -y --force-yes --no-install-recommends -t jessie -o Dpkg::Options::=\"--force-confold\" \
			$(TARGET_PACKAGES_JESSIE_OTHER)  $(TARGET_PACKAGES_JESSIE_X) $(TARGET_PACKAGES_JESSIE_FGLRX) \
			$(TARGET_PACKAGES_JESSIE_NVIDIA) $(TARGET_PACKAGES_JESSIE_SMARTCARD) ;\
		export PATH=/tmp:$$PATH ;\
		cp /bin/true /tmp/update-rc.d ;\
		cp /bin/true /tmp/invoke-rc.d ;\
		apt-get install -y --force-yes --no-install-recommends -t jessie -o Dpkg::Options::=\"--force-confold\" $(TARGET_PACKAGES_JESSIE_SNMP) ;\
		rm -f /tmp/update-rc.d /tmp/invoke-rc.d ;\
		(/etc/init.d/nfs-common stop || true) ;\
	"
	@touch $@

update-finish:
	make $@-stamp

update-finish-stamp:update-jessie-stamp $(TARGET_PACKAGES_DEB_AS_DEP)
	@echo "[1m Target @-stamp [0m"
	PATH=$(PATH) sudo  BIND_ROOT=./ Scripts/TCOS.chroot Filesystem $(SHELL) -c "\
		set -e ;\
		set -x ;\
                source /TCOS/Initrd/init_functions ;\
	        echo \"\$$GREEN############### installing remaining  packages ##################### \" \$$NORMAL ;\
		echo \"Installing prebuild openchrome ...\" ;\
                cp -ra /TCOS/Sources/openchrome-prebuild/usr / ;\
                cd /TCOS/Packages && dpkg -i  $(TARGET_PACKAGES_DEB) ;\
		wget https://fpdownload.macromedia.com/pub/labs/flashruntimes/flashplayer/linux32/libflashplayer.so -O \
			/usr/lib/mozilla/plugins/libflashplayer.so ;\
		ln -sf /usr/share/icons/hicolor/48x48/apps/firefox-esr.png /usr/share/icons/hicolor/48x48/apps/iceweasel.png ;\
		ln -sf /usr/lib/i386-linux-gnu/libdvdnav.so.4.1.2 /usr/lib/i386-linux-gnu/libdvdnavmini.so.4 ;\
		(/etc/init.d/nfs-common stop || true) ;\
	"
	@touch $@


busybox:
	make $@-stamp
busybox-stamp: update-finish-stamp
        # let's integrate this stuff in target driver to prevent installing build deps twice
	@echo "[1m Target busybox-stamp: Create the busybox[0m"
        # using submodules did never work - got to hell!
          # git submodule init
          # git submodule update
        # Patch for fbsplash: https://gitlab.c3sl.ufpr.br/cdn/tm-scripts/commit/c889d7d9df3a348dfc54e2f73aadf98ca6e025dc
	if [ ! -r Initrd/bin/busybox -o Sources/busybox.config -nt Initrd/bin/busybox ]; then\
		cd Sources &&\
		[ -d busybox ] && mv busybox busybox_`date +%s` &&\
		git clone --depth 1 --branch $(BUSYBOX_BRANCH) git://git.busybox.net/busybox &&\
		cp busybox.config busybox/.config &&\
		patch ./busybox/miscutils/fbsplash.c fbsplash-center.patch &&\
		cd .. &&\
		PATH=$(PATH) sudo AUFS=1 BIND_ROOT=./ Scripts/TCOS.chroot Filesystem $(SHELL) -c "\
			set -e ;\
			tcos-enable_jessie ;\
			DEBIAN_FRONTEND=noninteractive apt-get install -y --force-yes -o Dpkg::Options::=\"--force-confold\" make gcc bzip2 libc6-dev ;\
			cd /TCOS/Sources/busybox ;\
			make clean ;\
			make install ;\
			(/etc/init.d/nfs-common stop || true) ;\
		" ;\
		rm -f initrd-stamp ;\
	fi
	@touch $@

kernel:
	make $@-stamp

kernel-stamp: busybox-stamp
	@echo "[1m Target kernel-stamp: Install the kernel[0m"
#	test -d Base/base-$(BASE_VERSION) || mkdir -p Base/base-$(BASE_VERSION)
#	test -d Base/base-$(BASE_VERSION)/debian/base/sfs  || cp -ra Sources/Base_Skel/* Base/base-$(BASE_VERSION)/
#	test -d Base/base-$(BASE_VERSION)/debian/base/tftp || cp -ra Sources/Base_Skel/* Base/base-$(BASE_VERSION)/
	mkdir -p Base/base-$(BASE_VERSION)
	sudo cp -ra Sources/Base_Skel/* Base/base-$(BASE_VERSION)/
	test -L Base/base-$(BASE_VERSION)/tftp || (cd Base/base-$(BASE_VERSION); ln -snf debian/base/tftp tftp)
	test -L Base/base-$(BASE_VERSION)/sfs  || (cd Base/base-$(BASE_VERSION); ln -snf debian/base/sfs sfs)

	PATH=$(PATH) sudo BIND_ROOT=./ Scripts/TCOS.chroot Filesystem $(SHELL) -c "\
		set -e ;\
		export PATH=/tmp:$$PATH ;\
		tcos-enable_wheezy ;\
		cp /bin/true /tmp/update-initramfs ;\
		apt-get install -y --force-yes -t wheezy-backports \
			linux-image-$(TARGET_KERNEL_DEFAULT) \
			linux-image-$(TARGET_KERNEL_NONPAE) ;\
		tcos-enable_jessie ;\
		dpkg --add-architecture amd64; apt-get update ;\
		apt-get install -y --force-yes  -t jessie-backports linux-image-$(TARGET_KERNEL_64):amd64 ;\
		(/etc/init.d/nfs-common stop || true) ;\
	"
	sudo cp Filesystem/boot/vmlinuz*  Base/base-$(BASE_VERSION)/tftp/ ; \

	(cd Base/base-$(BASE_VERSION)/tftp/; \
		sudo mv vmlinuz-$(TARGET_KERNEL_DEFAULT) vmlinuz; \
		sudo mv vmlinuz-$(TARGET_KERNEL_NONPAE) vmlinuz_non-pae; \
		sudo mv vmlinuz-$(TARGET_KERNEL_64) vmlinuz_64; \
	)
	@touch $@

driver:
	make $@-stamp

driver-stamp:kernel-stamp 
	-rm -f compressed-stamp
	echo "[1m Target driver-stamp: Compile external modules for the kernel[0m"
	sudo rm -rf Driver && mkdir -p Driver/lib/modules
	sudo PATH=$(PATH) AUFS=1 BIND_ROOT=./ Scripts/TCOS.chroot Filesystem $(SHELL) -c "\
	source /TCOS/Initrd/init_functions && cp /bin/true /tmp/update-initramfs ;\
	set -e ;\
	export PATH=/tmp:$$PATH && export CONCURRENCY_LEVEL=4 ;\
        echo \"\$$GREEN############### installing DRVBUILD packages ##################### \" \$$NORMAL ;\
	tcos-enable_wheezy ;\
		apt-get install -y --force-yes --no-install-recommends -o Dpkg::Options::=\"--force-confold\" $(PACKAGES_WHEEZY_KHEADER) ;\
	tcos-enable_jessie ;\
		apt-get install -y --force-yes --no-install-recommends -o Dpkg::Options::=\"--force-confold\" $(PACKAGES_DKMS_DRVBUILD) ;\
	tcos-enable_wheezy ;\
		apt-get install -y --force-yes --no-install-recommends -o Dpkg::Options::=\"--force-confold\" $(PACKAGES_WHEEZY_DKMS) ;\
	tcos-enable_jessie ;\
		apt-get install -y --force-yes --no-install-recommends -o Dpkg::Options::=\"--force-confold\" $(PACKAGES_JESSIE_DKMS_FIRST_STEP) ;\
	rsync -vaR \
		/lib/modules/$(TARGET_KERNEL_DEFAULT)/updates /lib/modules/$(TARGET_KERNEL_DEFAULT)/modules.* \
		/lib/modules/$(TARGET_KERNEL_NONPAE)/updates  /lib/modules/$(TARGET_KERNEL_NONPAE)/modules.*  \
		/TCOS/Driver/  ;\
	apt-get purge -y $(PACKAGES_WHEEZY_DKMS) $(PACKAGES_JESSIE_DKMS_FIRST_STEP) || true ;\
	dpkg --add-architecture amd64; apt-get update ;\
	apt-get install -y --force-yes --no-install-recommends -o Dpkg::Options::=\"--force-confold\" $(PACKAGES_JESSIE_KHEADER) ;\
	apt-get install -y --force-yes --no-install-recommends -o Dpkg::Options::=\"--force-confold\" $(PACKAGES_JESSIE_DKMS_SECOND_STEP) ;\
	echo \"\$$GREEN############### installing USBRDR  ##################### \" \$$NORMAL ;\
		/TCOS/Scripts/TCOS.usbrdr_install  ;\
	rsync -vaR \
		/lib/modules/$(TARGET_KERNEL_DEFAULT)/updates /lib/modules/$(TARGET_KERNEL_DEFAULT)/modules.* \
		/lib/modules/$(TARGET_KERNEL_NONPAE)/updates  /lib/modules/$(TARGET_KERNEL_NONPAE)/modules.*  \
		/lib/modules/$(TARGET_KERNEL_64)/updates  /lib/modules/$(TARGET_KERNEL_64)/modules.*  \
		/TCOS/Driver/  ;\
	/etc/init.d/nfs-common stop || true; /etc/init.d/rpcbind stop || true ;\
	"

#	fglrx for 64 Bit kernel needs its own run
	sudo PATH=$(PATH) AUFS=1 BIND_ROOT=./ Scripts/TCOS.chroot Filesystem $(SHELL) -c "\
	source /TCOS/Initrd/init_functions && cp /bin/true /tmp/update-initramfs ;\
	set -e ;\
	export PATH=/tmp:$$PATH && export CONCURRENCY_LEVEL=4 ;\
        echo \"\$$GREEN############### installing fglrx-64Bit packages ##################### \" \$$NORMAL ;\
	tcos-enable_jessie ;\
		dpkg --add-architecture amd64; apt-get update ;\
		apt-get install -y --force-yes --no-install-recommends  $(PACKAGES_JESSIE_DMKS_BUILD64)  ;\
		apt-get install -y --force-yes --no-install-recommends fglrx-modules-dkms:amd64 ;\
	rsync -vaR \
		/lib/modules/$(TARGET_KERNEL_64)/updates  /lib/modules/$(TARGET_KERNEL_64)/modules.*  \
		/TCOS/Driver/  ;\
	/etc/init.d/nfs-common stop || true; /etc/init.d/rpcbind stop || true ;\
	"

#	Copy modules and binaries back to Filesystem	
	sudo rsync -va Driver/* Filesystem/

#	Renew module dependencies.
#	Keep in mind: depmod for all kernel is also triggered in target clean (script tcosify-clean)
	PATH=$(PATH) sudo BIND_ROOT=./ Scripts/TCOS.chroot Filesystem $(SHELL) -c "\
		set -x; for kernel in $(TARGET_KERNEL); do depmod \$$kernel; done \
	"
	@touch $@


initrd:
	make $@-stamp
initrd-stamp:busybox-stamp driver-stamp Sources/modules.list
	@echo "[1m Target $@: extract modules and compress initrd[0m"
#	Extract modules we need  inside initrd
	sudo TARGET_KERNEL="$(TARGET_KERNEL)" \
              TARGET_KERNEL_DEFAULT="$(TARGET_KERNEL_DEFAULT)" \
              TARGET_KERNEL_NONPAE="$(TARGET_KERNEL_NONPAE)" \
              TARGET_KERNEL_64="$(TARGET_KERNEL_64)" \
              FIRMWARE_PACKAGES="$(FIRMWARE_PACKAGES)" \
              SHELL=$(SHELL) BIND_ROOT=./ Scripts/TCOS.initrd
	sudo touch Initrd/etc/fstab

#	sudo $(SHELL) -e -c 'cd Initrd && find . | fakeroot cpio -H newc -o | xz -9 -T 0 --format=lzma > $$OLDPWD/Base/base-$(BASE_VERSION)/debian/base/tftp/initrd.img; cd ..'

	sudo rm -rf Initrd/devices
#	build/compress all the initrd-files we need.
	sudo $(SHELL) -e -c 'rsync -a Sources/busybox_apply_*/* Initrd/'
	sudo $(SHELL) -e -c 'cd Initrd && find . | fakeroot cpio -H newc -o | \
		xz -9 -T 0 --format=lzma > $$OLDPWD/Base/base-$(BASE_VERSION)/debian/base/tftp/initrd.img; cd ..'
	sudo $(SHELL) -e -c 'cd Initrd_firmware && find . | fakeroot cpio -H newc -o | \
	 	xz -9 -T 0 --format=lzma > $$OLDPWD/Base/base-$(BASE_VERSION)/debian/base/tftp/firmware.img; \
	 	cp $$OLDPWD/Base/base-$(BASE_VERSION)/debian/base/tftp/firmware.img \
	 	   $$OLDPWD/Base/base-$(BASE_VERSION)/debian/base/tftp/,firmware.img; cd ..'
	sudo $(SHELL) -e -c 'cd Initrd_firmware_empty && find . | fakeroot cpio -H newc -o | \
	 	xz -9 -T 0 --format=lzma > $$OLDPWD/Base/base-$(BASE_VERSION)/debian/base/tftp/firmware-empty.img; cd ..'
	sudo $(SHELL) -e -c 'cd Initrd_modules_vmlinuz  && find . | fakeroot cpio -H newc -o | \
		xz -9 -T 0 --format=lzma > $$OLDPWD/Base/base-$(BASE_VERSION)/debian/base/tftp/vmlinuz-modules.img; cd ..'
	sudo $(SHELL) -e -c 'cd Initrd_modules_vmlinuz_non-pae  && find . |fakeroot cpio -H newc -o | \
		xz -9 -T 0 --format=lzma > $$OLDPWD/Base/base-$(BASE_VERSION)/debian/base/tftp/vmlinuz_non-pae-modules.img; cd ..'
	sudo $(SHELL) -e -c 'cd Initrd_modules_vmlinuz_64  && find . | fakeroot cpio -H newc -o | \
		xz -9 -T 0 --format=lzma > $$OLDPWD/Base/base-$(BASE_VERSION)/debian/base/tftp/vmlinuz_64-modules.img; cd ..'
	@touch $@

init-test:
	sudo cp -ra Sources/tftp/* Base/base-$(BASE_VERSION)/tftp/
	sudo rm -rf Initrd/devices
	sudo rsync -va Sources/busybox_apply_*/* Initrd/
	sudo touch Initrd/etc/fstab
#       Read this about compression advantages and disadvantage
#       https://catchchallenger.first-world.info/wiki/Quick_Benchmark:_Gzip_vs_Bzip2_vs_LZMA_vs_XZ_vs_LZ4_vs_LZO
	sudo $(SHELL) -c 'cd Initrd && find . | fakeroot cpio -H newc -ov | xz -9 -T 0 --format=lzma > $$OLDPWD/Base/base-$(BASE_VERSION)/debian/base/tftp/initrd.img; cd ..'


clean:
	make $@-stamp
clean-stamp: initrd-stamp
	@echo "[1m Target clean-stamp: Clean up the filesystem[0m"
        # Delete useless packages here.
	PATH=$(PATH) sudo BIND_ROOT=./ Scripts/TCOS.chroot Filesystem $(SHELL) -c "\
		mv /etc/init.d/tcos_moved/* /etc/init.d/ ;\
		tcos-enable_wheezy ;\
		apt-get -y remove $(TARGET_PACKAGES_DELETE) ;\
		dpkg --purge $(TARGET_PACKAGES_DELETE) ;\
		apt-get -y autoremove  ;\
		find / -iname  \"*.dpkg-dist\" -exec rm -f {} \; ;\
		mkdir -p /etc/init.d/tcos_moved ;\
		for removeIt in $(RC_SCRIPT_DELETE); do update-rc.d -f \$$removeIt remove; mv /etc/init.d/\$$removeIt /etc/init.d/tcos_moved/; ln -s /bin/true /etc/init.d/\$$removeIt; done  ;\
		for disableService in $(DISABLED_SERVICES); do update-rc.d -f \$$disableService remove; done  ;\
	"
	-sudo TARGET_KERNEL="$(TARGET_KERNEL)" Scripts/TCOS.chroot Filesystem $(SHELL) < Scripts/TCOS.tcosify-clean
	@touch $@

compressed:
	make $@-stamp 

compressed-stamp:clean-stamp
	@echo "[1m Target compressed-stamp: Create the base.sfs container[0m"
#	have more recent firmware for a selection of hardware
	sudo $(SHELL) -c 'cd Sources/linux-firmware; rsync -a $(FIRMWARE_PACKAGES) ../../Filesystem/lib/firmware/;'
	nice -n +19 ionice -c 3 sudo XZ_OPT="-9" \
		mksquashfs Filesystem Base/base-$(BASE_VERSION)/sfs/base.sfs -ef \
			Sources/mksquashfs.excludes -wildcards -noappend -always-use-fragments -comp xz
	@touch $@

build-with-log:
	make dist-clean
	make compressed | tee base-build-$(BASE_VERSION)$(BASE_VERSION_MINOR).log
	@touch $@-stamp

# install-targets
# 
base-release: build-with-log-stamp
	@echo "[1m Target base-release. Creating the deb-package for delivery.[0m"
	sudo cp -ra Sources/tftp/* Base/base-$(BASE_VERSION)/tftp/
	sudo cp base-build-$(BASE_VERSION)$(BASE_VERSION_MINOR).log Base/base-$(BASE_VERSION)/sfs/
	USER=$(USER) PATH=$(PATH) sudo AUFS=1 BIND_ROOT=./ Scripts/TCOS.chroot Filesystem $(SHELL) -c " \
	    tcos-enable_jessie ;\
            DEBIAN_FRONTEND=noninteractive apt-get install -y --force-yes --no-install-recommends dpkg-dev fakeroot gcc build-essential debhelper ;\
            cd TCOS/Base/base-$(BASE_VERSION) ;\
	    touch debian/files ;\
	    echo 5 > debian/compat ;\
	    echo \"Please have your comments to changelog already done.\" ;\
	    cp debian/changelog ../base.changelog ;\
	    cp debian/changelog sfs/base.changelog ;\
	    dpkg-buildpackage -r\"fakeroot -u\" -us -uc -a i386 -b -Zgzip -z2 ;\
	"
	touch $@-stamp

upload-test: 
	@echo "[1m Target test: Copy base.sfs, kernel, etc. to development server for testing.[0m"
#	#-rsync -vaP Sources/tftp/*  $(LOCAL_TEST_PATH)/tftp/
	-rsync -vaP Base/base-$(BASE_VERSION)/tftp/* $(LOCAL_TEST_PATH)/tftp/
	-rsync -vaP Base/base-$(BASE_VERSION)/sfs/*.sfs     $(LOCAL_TEST_PATH)/sfs/
	-rsync -vaP --exclude='.empty' Base/base-$(BASE_VERSION)/custom/*      $(LOCAL_TEST_PATH)/custom/
#	-rsync -va Base/base-$(BASE_VERSION)/tftp/initrd.img  Base/base-$(BASE_VERSION)/tftp/initrd* $(LOCAL_TEST_PATH)/tftp/

dist-clean:
	-sudo rm -rf Filesystem
	-sudo rm -rf Driver
	-sudo rm -rf Kernel
	-sudo rm -rf Base/base-$(BASE_VERSION)/sfs/*
	-sudo rm -rf Base/base-$(BASE_VERSION)/tftp/*
	-sudo rm -f *-stamp

