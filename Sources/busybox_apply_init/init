#!/bin/bash

# set -x 

# LINBO busybox-based initramfs bootscript
# (C) 2013 -2017 by openthinclient.com
#
# LICENSE: GPL V3

# Its purpose:
# 1. Mounting a squashfs container over nfs
# 2. Unifying this ro conainter with ramdisk and file overlay
# 3. Hand over control to "real" sysv init. (DEBIAN)

export PATH=/bin:/usr/bin:/sbin:/usr/sbin
begin_time=$(date +%s)
TOP=$(pwd)

# bring functions in game
. $TOP/init_functions

# Some variable explanation
#
# TCOS                 - here it all begins
# LINK                 - nfs             OTC-Server:/openthinclient 
# WRITE                - for aufs/overlayfs to write 
# BASE                 - squashfs via loopback sfs-file: /tcos/link/sfs/base.sfs
# PACKAGE              - mount package-squashfs files here to subdirectories
# SHADOW               - the final folder for switchroot
# OVERLAY_WORK         - overlayfs needs a work dir
# OVERLAY_UPPER        - overlayfs needs a upper dir
# CUSTOM               - custom scripts
# NFSHOMES             - mount NFSHOMES folder to prepare report folder
# REPORTS              - Holds the reports. Folder exists only in the final debian rootfs!                 

export TCOS="/tcos" 
export LINK="$TCOS/link" \
       BASE="$TCOS/base" \
       PACKAGE="$TCOS/package"\
       SHADOW="$TCOS/shadow" \
       WRITE="$TCOS/write" \
       NFSHOMES="$TCOS/nfshomes" \
       REPORTS="$TCOS/reports"

export OVERLAY_WORK="$WRITE/overlay_work" \
       OVERLAY_UPPER="$WRITE/overlay_upper" \
       CUSTOM="$LINK/custom"


# Some more default variables here.
# Values can be overwritten in kernel commandline: Key=Value
#
console=tty1
redirect="/dev/tty1" 
fglrx=false
nvidia=false
splashImage=tcos-splash.ppm
ignore_dhcp=false
localboot=false
debug=false
multiIP=false
glamor=true
openchrome_DFP=false
openchrome_VBE=false
force_sna=false
splashFifo=/lib/splashscreens/fifo



######################################################

# have a a propper module.dep, it just takes half a second
depmod -a 

# create neccesary folders layering logic
# Note: The folder $OVERLAY_WORK resides inside $WORK and is created after mounting tmpfs on $WRITE
mkdir -p $LINK $WRITE $BASE $PACKAGE $SHADOW $CUSTOM $NFSHOMES

# some essential folders and mounts
#
#mkdir -p /var/lock /sys /proc /tmp /run /etc/modprobe.d
mkdir -p /var/lock /sys /proc /tmp /etc/modprobe.d 
#mkdir -p -m 0755 /dev /run/initramfs
mkdir -p -m 0755 /dev
mkdir -p -m 0700 /root
mount -t sysfs -o nodev,noexec,nosuid sysfs /sys 
mount -t devtmpfs -o rw,size=5M,mode=0755 devtmpfs /dev 
#mount -t tmpfs -o "nosuid,size=20%,mode=0755" tmpfs /run 
mount -t proc  -o nodev,noexec,nosuid proc /proc

# allow everyone to read dmesg
echo 0 > /proc/sys/kernel/dmesg_restrict 

# we need this for dropbear 
mkdir /dev/pts && mount -t devpts -o noexec,nosuid,gid=5,mode=0620 devpts /dev/pts  

# deploy a predefined set of dev files 
cp -a /devices/* /dev/

# redirect to /dev/null by default
[ -c /dev/null ] && redirect="/dev/null"

# Get Key=Value pairs from kernel commandline and overwrite defaults.
read CMDLINE < /proc/cmdline
getbootparams 

# generate /etc/modprobe.d/busybox_blacklist.conf dynamically
write_module_blacklist 

# load some essential modules in advance
modprobe -a -b overlay aufs nfs nfsv2 &> $redirect

# load modules for local disk if needed
$localboot && load_storage_modules

# let the kernel shut up if not in debug mode
# http://unix.stackexchange.com/questions/44999/how-can-i-hide-messages-of-udev/45525#45525
$debug || echo "0 0 0 0" > /proc/sys/kernel/printk  &> $redirect
echo 0 > /sys/class/graphics/fbcon/cursor_blink        # prevent cursor blinking

NFS=${nfsroot%:*}
LDAP=$(echo $ldapurl | egrep -o '([0-9]{1,3}\.){3}[0-9]{1,3}')

# send debug output to active console
$debug && redirect="/dev/${console}"

# Be extremly verbose if requested
grep -wq set_x /proc/cmdline && set -x

# Don't wait for nonexistent firmware
if [ ! -d /lib/firmware ] ; then
    echo 1 > /sys/class/firmware/timeout
fi

# generate /etc/modprobe.d/busybox_blacklist.conf dynamically
write_module_blacklist 

$debug && debugshell "before hardware auto-detect"

#######################################################
# MODULES stuff

$debug && echo "Loading kernel modules for detected hardware" 

# Have some special module parameter depending on detected HW
#
# Intel Corporation ValleyView Gen7 [8086:0f31]
# lspci | grep -qi 8086:0f31 && echo "options modeset=1" > /etc/modprobe.d/i915.conf

# Load kernel modules, run twice.
# Funktioniert u.U. nicht mit Geräten die keinen statischen Eintrag in der Kernel-Device-Tabelle haben.
# Einige modalias-files erscheinen erst, wenn ein Modul geladen ist. Henne-Ei. Das erklärt auch,
# weshalb ein zweiter Durchlauf u.U. weitere Module lädt.
find /sys -name 'modalias' -type f -exec cat '{}' + | sort -u | xargs modprobe -b -a 2>/dev/null
find /sys -name 'modalias' -type f -exec cat '{}' + | sort -u | xargs modprobe -b -a 2>/dev/null

# Whats's up? Have a stop here to interact.
$debug && debugshell "after hardware auto-detect"

# Always show figlet!
echo ${BLUE}; cat /lib/pales.figlet; echo ${NORMAL}

# Start splash after modules have been loaded to prevent resizing
# and/or flickering of the splashscreen.
# 
# Show the early splashscreen here (if no custom splash image is given)
#
[ "$splashImage" == "tcos-splash.ppm" -o "$splashImage" == "crazy" ] && \
    show_splashscreen &
echo 5 > $splashFifo

# provide a network address
get_dynIP
TCOS_MAC=$(cat /sys/class/net/$TCOS_NIC/address | sed 's/\://g')

# mount reports folder on remote NFS server
mount_reports 

echo 10 > $splashFifo

# loopback device  
ip addr add 127.0.0.1/8 dev lo
ip link set dev lo up

# mount NFS or local device for initial link
get_tcosLink
$debug && debugshell "after mounting /tcos/link ."

# Show the custom splashscreen here with some pic from NFS
[ "$splashImage" != "tcos-splash.ppm" ] && \
    show_splashscreen &
echo 20 > $splashFifo


###############################################################
# apply updates for localboot here
# 
# Keep in mind: The kernel and intrd (folder tftp) are updated 
#   within the the update script after the client has
#   successfully booted.
#   The sfs and custom folders will be applied here now.

# just for debug
# mount -o remount,rw $LINK; touch $LINK/lb_updates/.apply_updates

if [ $localboot -a -r $LINK/lb_updates/.apply_updates ]; then
    mount -o remount,rw $LINK                         # mount rw
    # for folder in sfs custom licenses; do
    #     echo -e "${GREEN}\t.. applying updates for $folder${NORMAL}"
    # 	rm -rf $LINK/$folder                          # remove old one
    # 	mv $LINK/lb_updates/$folder $LINK/            # have new one in place
    # 	cp -al $LINK/$folder $LINK/lb_updates/        # have a hardlink copy of the new one (for the next compare with rsync)
    # done
    # rm -f $LINK/lb_updates/.apply_updates             # reset update state

    echo -e "${GREEN}\t.. applying updates ${NORMAL}"
    cd $LINK
    for item in $(ls -1); do
	echo $item | grep -q -w -e lb_updates -e var || rm -rf $item;    # remove old content
    done
    cp -al $LINK/lb_updates/* $LINK/                  # have a hardlink copy of the new one 
    rm -f $LINK/lb_updates/.apply_updates             # reset update state

    mount -o remount,ro $LINK                         # mount read only again
    $debug && debugshell "after LB updates"
fi

echo 30 > $splashFifo

########################################################
# Mount SFS files, base and package-sfs 
# 
#

# always have  write layer
mount -t tmpfs -o mode=0755 none $WRITE # links writing branch to ram!

AUFS_TREE=""
OVERLAY_TREE=""

# mount base and packages sfs files
[ -f "$LINK/sfs/base.sfs" ] || (echo "${RED} could not find $LINK/sfs/base.sfs - exiting. ${NORMAL}" && exit 1)
if mount -n -t squashfs -o loop,ro "$LINK/sfs/base.sfs" $BASE 2> $redirect ; then
    echo "Mounting base.sfs ... ${GREEN}OK${NORMAL}"
    AUFS_TREE="${BASE}=ro" 
    OVERLAY_TREE="lowerdir="
else
    echo "base.sfs not mounted...${RED}ERROR${NORMAL}"
    debugshell "Could not mount base.sfs. This should not happen. Sorry!"
fi
echo 40 > $splashFifo

# mount all packages sfs-files now (in a function) 
mountsfs package

echo 50 > $splashFifo

# Check if we need aufs or overlayfs
if [ "$tcoskern" =  "vmlinuz_64" ]; then
    # modprobe overlay # https://www.kernel.org/doc/Documentation/filesystems/overlayfs.txt
    mkdir -p $OVERLAY_WORK $OVERLAY_UPPER # have this folders for overlayfs working propperly
    mount -t overlay overlay -o${OVERLAY_TREE}${BASE},upperdir=${OVERLAY_UPPER},workdir=${OVERLAY_WORK} ${SHADOW} || debugshell "Couldn't mount overlayfs to ${SHADOW}."
else
    # modprobe aufs
    mount -t aufs -o noplink,br:${WRITE}=rw:${AUFS_TREE} unionfs ${SHADOW}  || debugshell "Couldn't mount aufs to ${SHADOW}."
fi

$debug && debugshell "Debugshell after mounting aufs/overlay."

# do some things in background
mountcustom $CUSTOM &
echo 60 > $splashFifo

do_graphic_magic &
echo 70 > $splashFifo

mount --bind /sys ${SHADOW}/sys &
mount --bind /dev ${SHADOW}/dev &
mkdir -p ${SHADOW}/dev_from_init && mount --bind /dev ${SHADOW}/dev_from_init &
mount --bind /dev/pts ${SHADOW}/dev/pts &
mount --bind /proc ${SHADOW}/proc &

echo 80 > $splashFifo

# have this folders also inside the running system, just for debugging.
mkdir -p ${SHADOW}/$LINK ${SHADOW}/$BASE ${SHADOW}/$WRITE ${SHADOW}/${OVERLAY_UPPER} ${SHADOW}/${OVERLAY_WRITE} ${SHADOW}/${REPORTS} 
mount --bind $LINK                         ${SHADOW}/$LINK &
mount --bind $BASE                         ${SHADOW}/$BASE &
mount --bind $WRITE                        ${SHADOW}/$WRITE &
mount --bind $NFSHOMES/reports/$TCOS_MAC   ${SHADOW}/$REPORTS &
mount --bind $OVERLAY_UPPER                ${SHADOW}/$OVERLAY_UPPER 2> /dev/null &

if $localboot; then
    # this function overwrites the IP (if there was any)
    # with a fixed one (if there is any)
    get_fixedIP
    #check_ldapconnect $LDAP || debugshell "No LDAP connection possible. Network down? Please debug .."
    check_ldapconnect $LDAP || echo "${YELLOW}    Unable to connect LDAP-server. Using cached config values."
fi

echo 90 > $splashFifo

#######################################################
# Try to automatically use swap devices.
# lsblk reads old MBR and new GPT tables

#if $autoswap ; then
#    lsblk -P -o NAME,FSTYPE 2> /dev/null | grep swap | while read line; do 
#	eval "$line" 
#	swapon /dev/${NAME} &> /dev/null
#	[ $? -eq 0 ] && echo -e "activate swap on ${NAME} \t ${GREEN}OK${NORMAL}"
#    done
#fi

# convey busybox blacklist
[ -e /etc/modprobe.d/busybox_blacklist.conf ] && cp /etc/modprobe.d/busybox_blacklist.conf ${SHADOW}/etc/modprobe.d/

############################################################
#
# convey important DHCP answer options to wheezy

if [ ! -x ${LINK}/tcos-fixedIP.sh ]; then
    [ -e /tmp/udhcpc_vars ] && cat /tmp/udhcpc_vars >> ${SHADOW}/etc/environment &
    [ -e /etc/resolv.conf ] && cp /etc/resolv.conf ${SHADOW}/etc/ &
    [ -e /etc/hostname ] && cp /etc/hostname ${SHADOW}/etc/ &
    [ -e /etc/hosts ] && cp /etc/hosts ${SHADOW}/etc/ &
fi

[ -n "$TCOS_NIC" ] && echo "TCOS_NIC=$TCOS_NIC" >> ${SHADOW}/etc/environment

# let the kernel shut up if not in debug mode
# http://unix.stackexchange.com/questions/44999/how-can-i-hide-messages-of-udev/45525#45525
$debug || echo "0 0 0 0" > /proc/sys/kernel/printk  &> $redirect


# one more bindmount for each package
# otherwise it wouldn't appear inside the startet OS
#
for FOO in $(ls -1 $PACKAGE/); do (mkdir -p ${SHADOW}/${PACKAGE}/${FOO}; mount --bind ${PACKAGE}/${FOO} ${SHADOW}/${PACKAGE}/${FOO}) & done

# finish splasher
echo 100  > $splashFifo
echo exit > $splashFifo
touch /stop_splasher
killall -9 fbsplash &> /dev/null
ln -fs /dev/null /lib/splashscreens/fifo

# Wait for remaining functions running background and go ahead
wait

# fix some ownerships 
fixpermissions ${SHADOW} 

$debug && debugshell "last step before switch_root" 

#end_time=$(date +%s); let dur=${end_time}-${begin_time}
#echo "TCOS init took $dur seconds"

# debugshell "-------------"

echo -e "\n\n${GREEN}Calling Debian now.${NORMAL}\n\n"
exec switch_root ${SHADOW} /sbin/init "$@" </dev/${console} >/dev/${console} 2>&1

