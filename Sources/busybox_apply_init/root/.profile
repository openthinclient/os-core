
. /init_functions
PS1='\[\033[01;32m\]root\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '

alias ll='ls -lisa'
alias DE='tar -xzOf /lib/keymaps.tar.gz de-latin1-nodeadkeys.bmap | /sbin/loadkmap'
alias FR='tar -xzOf /lib/keymaps.tar.gz fr-latin1.bmap | /sbin/loadkmap'
alias R='reboot -f'

# Do not apologize if we're in debug mode already
if ! $debug; then
    echo -e ""
    echo -e "Unfortuantely, your computer could not boot the openthinclient OS."
    echo -e "We apologize for this inconvenience."
    echo -e ""
    echo -e "${WHITE}Please contact your system's administrator.${NORMAL}"
    echo -e ""
    echo -e "Should you be the administrator, you  can try the following troubleshooting"
    echo -e "assistance devices, which are free of charge:"
    echo -e ""
    echo -e "\twiki at ${WHITE}http://wiki.openthinclient.org.${NORMAL}"
    echo -e "\tmailing list at ${WHITE}openthinclient-user@lists.sourceforge.net${NORMAL}"
    echo -e ""
    echo -e "If you want professional support (for which we charge a service fee),"
    echo -e "please contact us at openthinclient via e-mail or phone."
    echo -e ""
    echo -e "\t${WHITE}info@openthinclient.com${NORMAL}"
    echo -e "\t${WHITE}+49 711 13786360${NORMAL}"
    echo -e ""
    echo -e "Please press any key."
    echo -e ""
    read bla
    echo -"${CLEAR}"
fi

echo ""
echo "This is a busybox debug shell (ash). Press TAB to list all available commands."
echo ""
echo -e "\t Type R and hit RETURN to reboot the system."
echo -e "\t Type CTRL+D to exit this shell and go on booting if possible."
echo
echo -e "\t Use commands \"DE\" or \"FR\" for german or french keyboard layout."
echo
